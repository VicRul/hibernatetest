package org.vicrul.hibernatetest;

import org.vicrul.hibernatetest.dao.CustomerDAO;
import org.vicrul.hibernatetest.dao.CustomerDAOImpl;
import org.vicrul.hibernatetest.entities.Customer;

import java.util.List;

public class RunApp {

    public static void main(String[] args) {
        CustomerDAO customerDAO = new CustomerDAOImpl();

        List<Customer> customers = customerDAO.customers();
        System.out.println(customers);
    }
}
