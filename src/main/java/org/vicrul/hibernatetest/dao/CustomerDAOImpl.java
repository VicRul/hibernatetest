package org.vicrul.hibernatetest.dao;

import org.hibernate.Session;
import org.vicrul.hibernatetest.entities.Customer;
import org.vicrul.hibernatetest.util.HibernateSessionUtilFactory;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO{

    private Session getSession(){
        return HibernateSessionUtilFactory.getSessionFactory().openSession();
    }

    @Override
    @Transactional
    public List<Customer> customers() {

        List<Customer> customers = new ArrayList<>();

        try(Session session = getSession()){
            session.beginTransaction();
            customers = session.createQuery("FROM Customer ").list();
            session.getTransaction().commit();
        }
        return customers;
    }
}
