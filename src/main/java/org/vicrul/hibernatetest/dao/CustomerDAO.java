package org.vicrul.hibernatetest.dao;

import org.vicrul.hibernatetest.entities.Customer;

import java.util.List;

public interface CustomerDAO {

    List<Customer> customers();
}
