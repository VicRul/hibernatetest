package org.vicrul.hibernatetest.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.registry.internal.StandardServiceRegistryImpl;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.vicrul.hibernatetest.entities.Customer;
import org.vicrul.hibernatetest.entities.Order;


public class HibernateSessionUtilFactory {
    private static SessionFactory sessionFactory;

    private HibernateSessionUtilFactory() {}

    public static SessionFactory getSessionFactory(){
        if (sessionFactory == null){
            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(Customer.class);
            configuration.addAnnotatedClass(Order.class);
            sessionFactory = configuration.buildSessionFactory();
        }

        return sessionFactory;
    }
}
